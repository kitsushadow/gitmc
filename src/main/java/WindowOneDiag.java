import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.GitCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;

public class WindowOneDiag extends JDialog  {
    public JPanel contentPane;
    public JButton buttonOK;
    public JButton buttonCancel;
    public JTextField textField1;
    public static String text;

    public WindowOneDiag() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();

            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        textField1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onTextField1();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK()  {
        Main.MCdir = textField1.getText();
        System.out.println(Main.MCdir);
        try {
            Git gitPrefix = Git.cloneRepository().setURI("https://bitbucket.org/nmd-project/modpack").setDirectory(new File(textField1.getText())).call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
        //gitPrefix.checkout().setCreateBranch(true).setName("new-branch").setStartPoint(prefixHash).call();
        // Cloning & checking out to postfix
        //Git gitPostfix = Git.cloneRepository().setURI(projectURL).setDirectory(new File(postfixPath)).call();
        //gitPostfix.checkout().setCreateBranch(true).setName("new-branch").setStartPoint(postfixHash).call();
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    private void onTextField1() {
        dispose();
    }

    public static void main(String[] args) {
        WindowOneDiag dialog = new WindowOneDiag();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
